import Vue from 'vue';
import * as types from './mutation-types';

export const getTodos = ({ commit }) => {
  commit(types.SET_LOADING, true);
  Vue.http.get('/api/v1/todos/').then((response) => {
    // 成功
    commit(types.SET_TODO, response);
    console.log("get success");
  }, (response) => {
    // エラー処理
    console.log('get failed')
  });
};

export const addTodos = ({ commit }, todo) => {
  Vue.http.post('/api/v1/todos', todo).then((response) => {
    // 成功
    console.log("add success");
  }, (response) => {
    // エラー処理
    console.log('add failed')
  })
};

export const deleteTodos = ({ commit }, id) => {
  Vue.http.delete('/api/v1/todos/' + id).then((response) => {
    // 成功
    console.log("delete success");
  }, (response) => {
    // エラー処理
    console.log('delete failed')
  });
};

export const updateTodos = ({ commit }, todo_update) => {
  Vue.http.put('/api/v1/todos/' + todo_update.id, todo_update.todo_update).then((response) => {
    // 成功
    console.log("update success");
  }, (response) => {
    // エラー処理
    console.log('update failed')
  })
};

