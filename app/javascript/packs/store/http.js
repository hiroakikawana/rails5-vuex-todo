import axios from 'axios';
import { SET_LOADING } from './mutation-types';

const http = axios;

export default (Vue, { store }) => {
  //リクエスト時にロード中と表示させるため。【参考】http://qiita.com/inuscript/items/ccb56b6fc05aa7821c42
  http.interceptors.request.use((config) => {
    store.commit(SET_LOADING, true);
    return config;
  }, (error) => {
    // エラー処理
  });

  //レスポンスでロード中を解除
  http.interceptors.response.use((response) => {
    store.commit(SET_LOADING, false);
    return response;
  }, (error) => {
    // エラー処理
  });

  Vue.http = http;
  Object.defineProperties(Vue.prototype, {
    $http: {
      get () {
        return http;
      }
    }
  });
};