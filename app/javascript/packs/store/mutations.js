import * as types from './mutation-types'; //mutation-typesをImportします

export const state = {
  message: 'hello vuex',
  todos: [],
  loading: false
};

export const mutations = {
  [types.MESSAGE_UPDATE] (state) {
    state.message = 'hello mutation';
  },
  // // 引数が必要な場合は第２引数に定義します
  // [types.HOGE] (state, param) {
  //   state.message = param.message;
  // },
  // // このような方法もよく見かけます（機能はHOGEと同じです）
  // [types.HUGA] (state, { message }) {
  //   state.message = message;
  // },
  [types.SET_TODO] (state, response) {
    state.todos = response.data;
  },
  [types.SET_LOADING] (state, isLoading) {
    state.loading = isLoading;
  }
};