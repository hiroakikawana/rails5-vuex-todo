import Vue from 'vue'
import store from './store'
import http from './store/http';
import App from './todo_app.vue'

Vue.use(http, { store }); //storeを引数に設定する

document.addEventListener('DOMContentLoaded', () => {
  document.body.appendChild(document.createElement('todo'))
  const app = new Vue({
    el: 'todo',
    store,
    template: '<App/>',
    components: { App }
  })
  console.log(app)
})
